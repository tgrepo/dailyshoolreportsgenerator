##Generator of blanks for the daily report of the behavior of a pupil in a school
###Usage
+ To modify lesson names open DailyRoportBlank.html with a text editor of your choice (notepad++) and modify the 'SCHED' variable (easy). The default values:

```javascript
      var SCHED = {
        sun: ["צרפתית",  "חינוך", "תרבות יהודית", "עברית", "של''ח", "אנגלית", "פיסול", "פיסול", "רישום", "רישום"],
        mon: ["עברית", "עברית", "מתמטיקה", "מתמטיקה", "היסטוריה", "גיאוגרפיה", "חנ''ג"],
        tue: ["צבע", "צבע", "תרבות יהודית", "אנגלית", "תנך", "תנך", "מדיה דיגיטלית", "מדיה דיגיטלית"],
        wed: ["כימיה", "היסטוריה", "מתמטיקה", "ביולוגיה", "ביולוגיה", "תנך", "גיאוגרפיה", "גיאוגרפיה"],
        thu: ["אנגלית", "אנגלית", "חנ''ג", "צרפתית", "צרפתית", "כימיה", "גיאוגרפיה", "תולדות האמנות", "תולדות האמנות"],
        fri: ["מתמטיקה", "מתמטיקה", "היסטוריה", "כימיה", "חינוך"]
      }
```

+ To create all blanks open PowerShell.exe, navigate to this repo folder and execute the script:
```PowerShell
    ./New-DailyReportBlanks.ps1
```

+ To create the blank for specific day use -Days flag with one of the set (sun,mon,tue,wed,thu,fri) parameters, for example:
```PowerShell
    ./New-DailyReportBlanks.ps1 -Days sun
```

+ To create the blanks for specific days use -Days flag with more than one of the set (sun,mon,tue,wed,thu,fri) parameters, for example:
```PowerShell
    ./New-DailyReportBlanks.ps1 -Days sun,wed,fri
```

###Prerequisites
+ PowerShell >=3 (tested with 5.1)
+ Google Chrome (tested with 72.0.3626.119)

