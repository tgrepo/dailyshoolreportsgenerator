[CmdletBinding()]
Param(
    [ValidateSet('sun','mon','tue','wed','thu','fri')]
    [string[]]$Days=('sun','mon','tue','wed','thu','fri')
)

Function Get-ChormeExeFullPath {
    $ChromePath = Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\chrome.exe'
    return $ChromePath.path + '\' + $ChromePath.PSChildName
}

function New-OutputFolder {
    $dirname = $($(Get-Location).Path + '\DailyReportBlanks')
    Remove-Item -Recurse $dirname -ErrorAction SilentlyContinue
    New-Item -ItemType Directory $dirname -ErrorAction Ignore | Out-Null
    return $dirname    
}

function Get-HtmlFileFullPath {
    return $PSScriptRoot + '/DailyReportBlank.html'    
}


$HtmlPath = Get-HtmlFileFullPath
$chrome = Get-ChormeExeFullPath
$OutputFolder = New-OutputFolder

foreach ($day in $Days) {
    $ofile = $OutputFolder + '\' + $day + '.pdf'
    $url="file:{0}?day={1}" -f ($HtmlPath -replace "\\", "/" -replace " ", "%20"), $day
    $params = "--headless","--print-to-pdf=`"$ofile`"","--no-margins",$url
    & "$chrome" $params
} 


